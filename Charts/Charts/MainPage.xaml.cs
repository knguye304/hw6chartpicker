﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;


namespace Charts
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
            CreateCharts();
            MainPicker.Items.Add("BarChart");
            MainPicker.Items.Add("DonutChart");
            MainPicker.Items.Add("LineChart");
          


        }

        public void CreateCharts()
        {
            var entryList = new List<Entry>()
            {
                new Entry(200)
                {
                    Color = SKColor.Parse("#FF1493"),
                    Label = "Dog Lovers",
                    ValueLabel="200"
                },
                new Entry(300)
                {
                    Color = SKColor.Parse("#00BFFF"),
                    Label = "Cat Lovers",
                    ValueLabel="200"
                },
                new Entry(400)
                {
                    Color = SKColor.Parse("#00CED1"),
                    Label = "Bird Lovers",
                    ValueLabel="200"
                }

            };
            var barChart = new BarChart() { Entries = entryList };
            var donutChart = new DonutChart() { Entries = entryList };
            var lineChart = new LineChart() { Entries = entryList };

            Chart1.Chart = barChart;
            Chart2.Chart = donutChart;
            Chart3.Chart = lineChart;

        }





        private void MainPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var name = MainPicker.Items[MainPicker.SelectedIndex];

            Chart1.IsVisible = false;
            Chart2.IsVisible = false;
            Chart3.IsVisible = false;

            if (name == "BarChart")
            {
                Chart1.IsVisible = !Chart1.IsVisible;
            }
            else if (name == "DonutChart")
            {
                Chart2.IsVisible = !Chart2.IsVisible;
         
            }
            else if (name == "LineChart")
            {
                Chart3.IsVisible = !Chart3.IsVisible;
            }
            else
            {
                ;
            }

            
        }
    }
}
